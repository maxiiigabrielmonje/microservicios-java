package com.example.orders_service.dto;

import com.example.orders_service.model.Order;

import java.util.List;

public record OrderResponse(Long id, String orderNumber, List<OrderItemsResponse> orderItems) {
}
