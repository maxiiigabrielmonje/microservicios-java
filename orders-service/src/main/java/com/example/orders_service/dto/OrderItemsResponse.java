package com.example.orders_service.dto;

import java.math.BigDecimal;

public record OrderItemsResponse(Long id, String sku, Double price, Long quantity) {
}
